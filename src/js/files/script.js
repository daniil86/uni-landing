// Подключение функционала "Чертогов Фрилансера"
import { isMobile } from "./functions.js";


//========================================================================================================================================================
// header
// Исключаем случаи, когда меню наезжает на шапку - проверяем соотношение сторон и добавляем отступ сверху в определенных случаях
const headerConfig = {
	isStartPage: true
}

function checkWindowSize(block) {
	const ratio = innerWidth / innerHeight;
	console.log(ratio);
	if (ratio > 2.05) document.querySelector(block).style.marginTop = '100px';
	if (ratio > 1.9 && ratio <= 2.05) document.querySelector(block).style.marginTop = '60px';
	if (ratio > 1.7 && ratio <= 1.9) document.querySelector(block).style.marginTop = '30px';
}

// window.addEventListener('resize', () => {
// 	checkWindowSize('.menu__list');
// })
// checkWindowSize('.menu__list');

//========================================================================================================================================================
// volume
const volumeConfig = {
	isActive: false, // данный указатель запрещает любые звуки. Это значение при загрузке. После клика в любое место - меняем на true и запускаем звуки
	playing: true
}

//========================================================================================================================================================
// book
const prevBtn = document.querySelector('.book__prevBtn');
const nextBtn = document.querySelector('.book__nextBtn');
const book = document.querySelector('.book__papers');

const paper1 = document.querySelector('#p1');
const paper2 = document.querySelector('#p2');
const paper3 = document.querySelector('#p3');

const logoHeader = document.querySelector('.header__logo');

prevBtn.addEventListener('click', goPrevPage);
nextBtn.addEventListener('click', goNextPage);

let currentLocation = 1;
let numOfPapers = 3;
let maxLocation = numOfPapers + 1;

function openBook() {
	book.style.transform = 'translateX(50%)';
	prevBtn.style.transform = 'translateX(-50px)';
	nextBtn.style.transform = 'translateX(50px) rotate(180deg)';
}

function closeBook(isAtBeginning) {
	if (isAtBeginning) {
		book.style.transform = 'translateX(0)';
	} else {
		book.style.transform = 'translateX(100%)';
	}

	prevBtn.style.transform = 'translateX(0)';
	nextBtn.style.transform = 'translateX(0) rotate(180deg)';
}

function goNextPage() {
	if (currentLocation < maxLocation) {
		switch (currentLocation) {
			case 1:
				openBook();
				paper1.classList.add('flipped');
				setTimeout(() => {
					paper1.style.zIndex = 1;
				}, 500);
				logoHeader.classList.add('_visible');
				break;
			case 2:
				paper2.classList.add('flipped');
				setTimeout(() => {
					paper2.style.zIndex = 2;
				}, 500);

				break;
			case 3:
				paper3.classList.add('flipped');
				paper3.style.zIndex = 3;
				closeBook();
				break;
			default:
				throw new Error('unknown state');
		}
		currentLocation++;
	}
}

function goPrevPage() {
	if (currentLocation > 1) {
		switch (currentLocation) {
			case 2:
				closeBook(true);
				paper1.classList.remove('flipped');
				paper1.style.zIndex = 3;
				logoHeader.classList.remove('_visible');
				break;
			case 3:
				paper2.classList.remove('flipped');

				setTimeout(() => {
					paper2.style.zIndex = 2;
				}, 900);
				break;
			case 4:
				openBook();
				paper3.classList.remove('flipped');

				setTimeout(() => {
					paper3.style.zIndex = 1;
				}, 900);
				break;
			default:
				throw new Error('unknown state');
		}
		currentLocation--;
	}
}

//========================================================================================================================================================
//events
document.addEventListener('click', (e) => {
	const targetElement = e.target;
	if (targetElement.closest('.volume') && volumeConfig.playing) {
		targetElement.closest('.volume').classList.add('_disabled');
		setTimeout(() => {
			volumeConfig.playing = false;
		}, 0);
	}
	if (targetElement.closest('.volume') && !volumeConfig.playing && targetElement.closest('.volume').classList.contains('_disabled')) {
		targetElement.closest('.volume').classList.remove('_disabled');
		setTimeout(() => {
			volumeConfig.playing = true;
		}, 0);
	}
})